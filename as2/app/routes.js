module.exports = function(app) {

	// server routes ===========================================================
	// handle things like api calls
	// api ---------------------------------------------------------------------
	var Star = require('./models/Star')
	// routes ==================================================
	// get all stars
	app.get('/api/stars', function(req, res) {

			Star.find().sort({ likes: 'desc' }).exec(function(err, stars) {
					if (err)
							res.send(err)

					res.json(stars); // return all todos in JSON format
			});
	});

	app.post('/api/stars', function(req, res) {

	        Star.create({
	            serieid : req.body.serieid,
	            likes : 0
	        }, function(err, star) {
	            if (err)
	                res.send(err);

	            Star.find(function(err, stars) {
	                if (err)
	                    res.send(err)
	                res.json(stars);
	            });
	        });

	    });

			app.get('/api/stars/:id', function(req, res) {

					Star.findOne({serieid : req.params.id}, function(err, star) {

							if (err)
									res.send(err);
													// create star if it doesn't exist
	                        if(!star) {
																console.log("no star found")
																Star.create({
																		serieid : req.params.id,
																		likes : 0
																}, function(err, star) {
																		if (err)
																				res.send(err);
																				res.json(star);
																});
	                            }
													else {
														res.json(star);
													}
							 // return all todos in JSON format
					});
			});

			app.put('/api/stars/:id', function(req, res) {

					Star.findOne({serieid : req.params.id}, function(err, star) {

							if (err)
									res.send(err);
							console.log("gonna updates star")
							star.likes += 1;
	star.save(function(err) {
	  if (err) throw err;

	  console.log('User saved successfully!');
	});
							res.json(star); // return all todos in JSON format
					});
			});

	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});

};
