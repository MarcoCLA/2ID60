/* function fo filter empty json objects otherwhise .text would output [object Object] */
jQuery.fn.extend({
  textfilter: function(object) {
    var objectout = ''
  if(jQuery.isEmptyObject(object)) {
    return this.text("No content in database");
  }
  else{
    return this.text(object);
  }
  }
})

$(document).ready(function() {
    function getSearch($searchstr) {
        var result;
        var $seriesid;
        var $data = $.ajax({
            url: "php/proxyXML.php?url=http://thetvdb.com/api/GetSeries.php?seriesname=" + $searchstr,
            dataType: 'json',
            success: function(data) {
                var result = data["Series"];
                $( "#episodes" ).empty();
                $("#secondcol").empty();
                if ($.isArray(result)) {
                    result = result[0]
                }
                var banner = "http://thetvdb.com/banners/_cache/" + result["banner"]
                $("#secondcol").append("<img id='seriesbanner' class='img-responsive' src='" + banner + "'/><br>");
                $seriesid = result["seriesid"];
                $("#seriesid").text($seriesid);
                $("#overview").text(result["Overview"]);
                getSeriesLikes($seriesid)
                getSeriesData($seriesid)
            }
        });

    };

    function getSeriesData($id) {
        $.ajax({
            url: "php/proxyXML.php?url=http://thetvdb.com/api/E75FFD14EC33CDF9/series/" + $id + "/all/en.xml",
            dataType: 'json',
            success: function(data) {
                var result = data["Episode"];
                var seriesall = data["Series"];
                /* Check if episode data really is an array and append all the episodes*/
                if ($.isArray(result)) {
                    jQuery.each(result, function(i, episode) {
                        /*var episodediv = $("<div>").text(episode.SeasonNumber + "." + episode.EpisodeNumber + " " + episode.EpisodeName).addClass( "episode" );
                        var episodetotal = $("<div>").text(episode.Overview).addClass( "episodetotal" ).hide();
                        episodediv.append(episodetotal);
                        $("#episodes").append(episodediv);*/
                        var episodetr = $("<tr>").addClass("episode");
                        episodetr.append($("<th>").text(episode.SeasonNumber + "." + episode.EpisodeNumber));
                        episodetr.append($("<td>").text(episode.EpisodeName));
                        episodetr.append($("<td>").text(episode.FirstAired));
                        var episodetroverview = $("<tr>").addClass( "episodetotal" ).hide();
                        episodetroverview.append($("<td colspan=3>").textfilter(episode.Overview));
                        console.log("add episode")
                        $("#episodes").append(episodetr);
                        $("#episodes").append(episodetroverview);
                    });
                    /* adding rating and imdb link, can not be obtained from getseries */
                    $("#secondcol").append($("<p>").text("Rating: " + seriesall.Rating));
                    console.log("add imdb");
                    var link = "<a href='http://www.imdb.com/title/" + seriesall.IMDB_ID + "'>IMDB LINK</a>"
                    $("#secondcol").append(link);
                } else {

                }
                /* Function to open and close the episodes overviews */
                $("tr.episode").click(function() {
                    console.log("Click episode")
                    $(this).toggleClass( "danger" )
                    $(this).next().toggle();
                });
                return data
            }
        });
    };

    function getSeriesLikes($id) {
        $.ajax({
            url: "api/stars/" + $id,
            dataType: 'json',
            success: function(data) {
                var likes = data["likes"];
                console.log(likes)
                $("likes").text(likes);
            }
        });
    };

    function addSeriesLikes($id) {
        $.ajax({
            url: "api/stars/" + $id,
            type: 'PUT',
            dataType: 'json',
            success: function(data) {
                var likes = data["likes"];
                console.log(likes)
                $("likes").text(likes);
            }
        });
    };
    /* function to do the initial search function */
    $("button").click(function() {
        var value = $("input").val();
        var $seriesid = getSearch(value)
        var $seriesdata = getSeriesData($seriesid)
    });
    $(".arrow-up").click(function() {
        var $seriesid = $("#seriesid").text();
        console.log($seriesid )
        addSeriesLikes($seriesid)
    });

});
