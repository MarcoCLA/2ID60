/* function fo filter empty json objects otherwhise .text would output [object Object] */
jQuery.fn.extend({
  textfilter: function(object) {
    var objectout = ''
  if(jQuery.isEmptyObject(object)) {
    return this.text("No content in database");
  }
  else{
    return this.text(object);
  }
  }
})

$(document).ready(function() {
  function getSeriesName($id) {
      $.ajax({
          url: "php/proxyXML.php?url=http://thetvdb.com/api/E75FFD14EC33CDF9/series/" + $id + "/all/en.xml",
          dataType: 'json',
          success: function(data) {
              console.log(data)
              var $seriesname = data["Series"];
              console.log($seriesname.SeriesName)
              $('#' + $id).text($seriesname.SeriesName)
              return $seriesname
          }
      });
  };

    function getLikes() {
        var result;
        var $seriesid;
        var $data = $.ajax({
            url: "api/stars",
            dataType: 'json',
            success: function(data) {
              console.log(data)
              jQuery.each(data, function(i, series) {
                  console.log(series.serieid)
                  var $seriesid = series.serieid
                  var seriestr = $("<tr>").addClass("episode");
                  seriestr.append($("<th>").text(series.serieid).attr('id', series.serieid));
                  seriestr.append($("<td>").text(series.likes));
                  $("#likeslist").append(seriestr);
                  var seriesname = getSeriesName($seriesid)
              });
            }
        });

    };
    getLikes();
});
