'use strict';

/* App Module */

var serieApp = angular.module('serieApp', [
  'ngRoute',
  'serieControllers',
  'serieFilters',
  'serieServices'
]);

serieApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/phones', {
        templateUrl: 'views/serie.html',
        controller: 'PhoneListCtrl'
      }).
      when('/phones/:phoneId', {
        templateUrl: 'partials/phone-detail.html',
        controller: 'PhoneDetailCtrl'
      }).
      otherwise({
        redirectTo: '/phones'
      });
  }]);
